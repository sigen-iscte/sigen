'use strict';
module.exports = function (gulp, conf) {

  var del = require('del');

  // Clean Output Directory
  gulp.task('clean', function (cb) {
    del([
      conf.assetsPath + conf.cssPath,
      conf.assetsPath + conf.fontsPath,
      conf.assetsPath + conf.imgPath,
      conf.assetsPath + conf.jsPath
    ], cb);
  });

};
