'use strict';
module.exports = function (gulp, conf) {

  var browserSync = require('browser-sync');
  var watch = require('gulp-watch');

  // Browsersync
  var bsEnabled = conf.bsEnabled || false;
  var bsReload = bsEnabled ? browserSync.reload : function () {};
  var bsUrl = conf.bsProxy;


  // Watch Files For Changes & Reload
  gulp.task('serve', function () {
    if (bsEnabled) {
      browserSync({
        proxy: bsUrl,
        open: false
      });

      watch(conf.templatesPath + '**/*' + conf.templatesExt, function () {
        gulp.start('templates-watch');
      });
    }

    watch(conf.devAssetsPath + conf.sassPath + '**/*.{sass,scss}', function () {
      gulp.start('sass-watch');
    });
    watch(conf.devAssetsPath + conf.jsPath + '**/*.js', function () {
      gulp.start('js-watch');
    });
    watch(conf.devAssetsPath + conf.imgPath + '**/*.{jpg,jpeg,gif,png,svg}', function () {
      gulp.start('images-watch');
    });
  });

  gulp.task('templates-watch', bsReload);
  gulp.task('sass-watch', ['styles'], bsReload);
  gulp.task('js-watch', ['js'], bsReload);
  gulp.task('images-watch', ['images'], bsReload);
};
