'use strict';
module.exports = function (gulp, conf) {

  // Build Production Files
  gulp.task('build', ['clean'], function () {
    gulp.start('styles');
    gulp.start('js');
    gulp.start('images');
    gulp.start('fonts');
  });
};
