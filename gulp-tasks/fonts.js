'use strict';
module.exports = function (gulp, conf) {

  var size = require('gulp-size');

  // Copy Web Fonts To Dist
  gulp.task('fonts:copy', function () {
    return gulp.src(conf.devAssetsPath + conf.fontsPath + '**')
      .pipe(gulp.dest(conf.assetsPath + conf.fontsPath))
      .pipe(size({title: 'fonts'}));
  });

  gulp.task('fonts', ['fonts:copy']);

};
