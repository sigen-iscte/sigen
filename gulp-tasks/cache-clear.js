'use strict';
module.exports = function (gulp, conf) {

  var cache = require('gulp-cache');

  // Clear Cache
  gulp.task('cache:clear', function (cb) {
    return cache.clearAll(cb);
  });

};
