'use strict';
module.exports = function (gulp, conf) {

  var cache = require('gulp-cache');
  var imagemin = require('gulp-imagemin');
  var pngquant = require('imagemin-pngquant');
  var size = require('gulp-size');

  var pngquantConf = conf.pngquant || {quality: '70-85', speed: 4};
  var svgoConf = conf.svgo || [{removeUselessDefs: false}, {cleanupIDs: false}];

  // Optimize Images
  gulp.task('images', function () {
    return gulp.src(conf.devAssetsPath + conf.imgPath + '**/*.{jpg,jpeg,gif,png,svg}')
      .pipe(size({title: 'images before'}))
      .pipe(cache(imagemin({
        progressive: true,
        interlaced: true,
        svgoPlugins: svgoConf,
        use: [pngquant(pngquantConf)]
      })))
      .pipe(gulp.dest(conf.assetsPath + conf.imgPath))
      .pipe(size({title: 'images after'}));
  });

};
