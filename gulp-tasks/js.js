'use strict';
module.exports = function (gulp, conf) {

  var cache = require('gulp-cache');
  var jshint = require('gulp-jshint');
  var size = require ('gulp-size');
  var uglify = require('gulp-uglify');

  // Lint JavaScript
  gulp.task('js:hint', function () {
    return gulp.src(conf.devAssetsPath + conf.jsPath + '**/*.js')
      .pipe(jshint())
      .pipe(jshint.reporter('jshint-stylish'))
      .pipe(jshint.reporter('fail'));
  });

  // Optimize JS
  gulp.task('js:optimize', ['js:hint'], function () {
    return gulp.src(conf.devAssetsPath + conf.jsPath + '**/*.js')
      .pipe(cache(uglify()))
      .pipe(gulp.dest(conf.assetsPath + conf.jsPath))
      .pipe(size({title: 'javascript'}));
  });

  gulp.task('js', ['js:optimize']);

};
