'use strict';

module.exports = function (gulp, conf) {
  gulp.task('dist', function () {
    gulp.start('build');
    gulp.start('php-cs-fixer');
  });
};
