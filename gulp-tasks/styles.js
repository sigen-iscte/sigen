'use strict';
module.exports = function (gulp, conf) {

  var autoprefixer = require('gulp-autoprefixer');
  var csso = require('gulp-csso');
  var util = require('gulp-util');
  var sass = require('gulp-sass');
  var size = require('gulp-size');

  // Autoprefixer
  var prefixerConf = conf.prefixer || ['last 2 version', 'ie 8', 'ie 9'];

  gulp.task('styles:scss', function () {
    return gulp.src(conf.devAssetsPath + conf.sassPath + '**/*.{sass,scss}')
      .pipe(sass({
        'onError': util.log
      }))
      .pipe(autoprefixer(prefixerConf))
      .pipe(csso())
      .pipe(gulp.dest(conf.assetsPath + conf.cssPath))
      .pipe(size({title: 'css'}));
  });

  // Output Final CSS Styles
  gulp.task('styles', ['styles:scss']);

};
