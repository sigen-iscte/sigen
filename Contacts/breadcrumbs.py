from django.core.urlresolvers import reverse


def lists(request):
    request.breadcrumbs("Listas", reverse("lists"))


def list_id(request, obj):
    lists(request)
    if obj.id:
        request.breadcrumbs(obj.name, reverse('list', args=(obj.id,)))
    else:
        request.breadcrumbs(obj.name, reverse('lists'))


def contact(request, the_list, obj):
    list_id(request, the_list)
    if obj.id:
        request.breadcrumbs(obj.name, reverse('contact', args=(the_list.id, obj.id)))
    else:
        request.breadcrumbs(obj.name, reverse('contact', args=(the_list.id,)))
