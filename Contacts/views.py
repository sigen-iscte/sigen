from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from Contacts import breadcrumbs
from Contacts.forms import EditContact, EditList
from Contacts.models import ContactList, Contact
from Newsletter.models import Newsletter, Recipient


@login_required
def lists(request):
    content = {
        'lists': ContactList.objects.filter(owner=request.user),
    }

    breadcrumbs.lists(request)
    return render(request, 'Contacts/lists.html', content)


@login_required
def lists_id(request, list_id=None):
    the_list = ContactList.objects.get(pk=list_id, owner=request.user) \
        if ContactList.objects.filter(pk=list_id, owner=request.user).exists() \
        else ContactList(owner=request.user)
    form = EditList(request.POST or None, instance=the_list)

    if request.POST and form.is_valid():
        form.full_clean()
        form.save()
        return HttpResponseRedirect(reverse('lists'))

    breadcrumbs.list_id(request, the_list)
    return render(request, 'Contacts/list.html', {
        'list': the_list,
        'form': form,
    })


@login_required
def remove_list(request, list_id):
    get_object_or_404(ContactList, pk=list_id, owner=request.user).delete()
    return HttpResponseRedirect(reverse('lists'))


@login_required
def contact(request, list_id, contact_id=None):
    contact_list = get_object_or_404(ContactList, pk=list_id, owner=request.user)
    the_contact = Contact.objects.get(pk=contact_id, contact_list=contact_list) \
        if Contact.objects.filter(pk=contact_id, contact_list=contact_list).exists() \
        else Contact(contact_list=contact_list)
    form = EditContact(request.POST or None, instance=the_contact)

    if request.POST and form.is_valid():
        form.full_clean()
        form.save()
        return HttpResponseRedirect(reverse('list', args=(contact_list.pk,)))

    breadcrumbs.contact(request, contact_list, the_contact)
    return render(request, 'Contacts/contact.html', {
        'contact': the_contact,
        'form': form,
        'newsletters_as_recipient': Newsletter.get_newsletters_for_contact(contact_id),
        'number_of_fails': Recipient.get_number_of_fails_for_contact(contact_id),
    })
