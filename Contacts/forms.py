from django.forms import ModelForm

from Contacts.models import Contact, ContactList


class EditContact(ModelForm):
    class Meta:
        model = Contact
        fields = ('name', 'email', 'active')
        labels = {
            'name': 'Nome',
            'email': 'Email',
            'active': 'Activo',
        }


class EditList(ModelForm):
    class Meta:
        model = ContactList
        fields = ('name',)
        labels = {
            'name': 'Nome',
        }
