from django.contrib import admin

# Register your models here.
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from Contacts.models import Contact, ContactList


@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'contact_list', 'unsubscribe_link')
    list_filter = ('contact_list',)
    list_select_related = ('contact_list',)
    search_fields = ('name', 'email')

    def unsubscribe_link(self, obj):
        return format_html(
            '<a href="{}?token={}">{}</a>',
            reverse('unsubscribe'),
            obj.token,
            'Click here to unsubscribe'
        )


@admin.register(ContactList)
class ContactListAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_in', 'updated_in', 'contacts_link')

    def contacts_link(self, obj):
        return format_html(
            '<a href="{}?contact_list__id__exact={}">{}</a>',
            reverse('admin:Contacts_contact_changelist'),
            obj.id,
            'Show Contacts'
        )

    contacts_link.short_description = 'Show Contacts'
