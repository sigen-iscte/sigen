from django.conf.urls import url

import Contacts.views

urlpatterns = [
    url(r'^lists/$', Contacts.views.lists, name='lists'),
    url(r'^lists/edit/([0-9]+)?', Contacts.views.lists_id, name='list'),
    url(r'^lists/([0-9]+)/remove$', Contacts.views.remove_list, name='list-delete'),
    url(r'^lists/([0-9]+)/([0-9]+)?$', Contacts.views.contact, name='contact'),
]
