import uuid

from django.contrib.auth.models import User
from django.db import models

from django.utils.timezone import now


class ContactList(models.Model):
    name = models.CharField(max_length=255)
    owner = models.ForeignKey(User)
    created_in = models.DateTimeField(default=now, editable=False)
    updated_in = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']

    def size(self):
        return Contact.objects.filter(contact_list=self).count()

    def contacts(self):
        return Contact.objects.filter(contact_list=self)

    def __str__(self):
        return self.name


class Contact(models.Model):
    contact_list = models.ForeignKey(ContactList, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    active = models.BooleanField(default=True)
    token = models.UUIDField(default=uuid.uuid4, editable=False)

    class Meta:
        ordering = ['-id']

    def deactivate(self):
        self.active = False

    def __str__(self):
        return self.name
