from django.core.urlresolvers import reverse

from Contacts import breadcrumbs


def Import(request, string):
    breadcrumbs.lists(request)
    request.breadcrumbs(string, reverse("import" + string))
