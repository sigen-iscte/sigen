from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^importCSV/$', views.import_csv, name='importCSV'),
    url(r'^importSQL/$', views.import_sql, name='importSQL'),
]
