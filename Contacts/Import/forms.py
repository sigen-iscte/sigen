import csv

import codecs
from django import forms
from django.db import connections

from Contacts.models import ContactList, Contact


class BASE(forms.Form):
    list_name = forms.CharField(label='Nome da Lista', max_length=254)

    def save(self, request, data):
        contact_list = ContactList.objects.create(name=self.cleaned_data['list_name'], owner=request.user)

        Contact.objects.bulk_create([Contact(
            name=row['name'],
            email=row['email'],
            contact_list=contact_list
        ) for row in data])


class CSV(BASE):
    name = forms.CharField(max_length=254, initial='name')
    email = forms.CharField(max_length=254, initial='email')
    delimiter = forms.CharField(
        max_length=1,
        min_length=1,
        initial=',',
        label="Delimiter do ficheiro csv",

    )
    file = forms.FileField(label='Ficheiro a importar')
    has_header = forms.BooleanField(label='O seu ficheiro tem header?', required=False)

    def save(self, request):
        fields = None if self.cleaned_data['has_header'] else {'name', 'email'}
        dic = csv.DictReader(
            codecs.iterdecode(self.cleaned_data['file'], 'utf-8'),
            delimiter=self.cleaned_data['delimiter'],
            fieldnames=fields
        )
        try:
            data = [{
                        'name': row[self.cleaned_data['name']],
                        'email': row[self.cleaned_data['email']],
                    } for row in dic]
            super().save(request, data)
        except KeyError:
            pass


class SQL(BASE):
    backend = forms.ChoiceField(choices=(
        ('mysql', 'Mysql'),
        ('postgressql', 'Postgres'),
        ('oracle', 'Oracle'),
    ))
    hostname = forms.CharField(max_length=254)
    port = forms.CharField(max_length=254, initial=3306)
    database = forms.CharField(max_length=254)
    username = forms.CharField(max_length=254)
    password = forms.CharField(max_length=254)
    query = forms.CharField(
        widget=forms.Textarea,
        help_text='Tem de haver um campo chamado "name" e um campo chamado "email" para ser correctamente importado'
    )

    def save(self, request):
        connections.databases['new-connection'] = {
            'ENGINE': 'django.db.backends.%s' % self.cleaned_data['backend'],
            'NAME': self.cleaned_data['database'],
            'USER': self.cleaned_data['username'],
            'PASSWORD': self.cleaned_data['password'],
            'HOST': self.cleaned_data['hostname'],
            'PORT': self.cleaned_data['port'],
        }
        conn = connections['new-connection']

        cursor = conn.cursor()
        cursor.execute(self.data['query'])
        super().save(request, [dict(zip([col[0] for col in cursor.description], row)) for row in cursor.fetchall()])
