from django.apps import AppConfig


class ImportfromcsvConfig(AppConfig):
    name = 'Contacts.Import'
