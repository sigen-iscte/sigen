from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render

from Contacts.Import import breadcrumbs
from Contacts.Import.forms import CSV, SQL


@login_required
def import_csv(request):
    content = {
        'form': CSV(request.POST or None, request.FILES or None),
    }

    breadcrumbs.Import(request, "CSV")
    if request.POST and request.FILES:
        if content['form'].is_valid:
            content['form'].full_clean()
            content['error'] = content['form'].save(request)
            if content['error'] is None:
                return HttpResponseRedirect(reverse('lists'))
    return render(request, "shared/centered_form.html", content)


@login_required
def import_sql(request):
    content = {
        'form': SQL(request.POST or None),
    }

    breadcrumbs.Import(request, "SQL")
    if request.POST:
        if content['form'].is_valid:
            content['form'].full_clean()
            content['error'] = content['form'].save(request)
            if content['error'] is None:
                return HttpResponseRedirect(reverse('lists'))
    return render(request, "shared/centered_form.html", content)
