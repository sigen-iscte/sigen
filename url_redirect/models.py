import uuid

from django.db import models
from django.db.models.aggregates import Count
from django.db.models.expressions import F
from django.utils.timezone import now

from Newsletter.models import Recipient


class URL(models.Model):
    token = models.UUIDField(default=uuid.uuid4, editable=False)
    url = models.URLField()
    recipient = models.ForeignKey(Recipient, on_delete=models.CASCADE)
    unsubscrive = models.BooleanField(default=False)

    def __str__(self):
        return str(self.token)

    def is_unsubscrive(self):
        return self.unsubscrive


class Access(models.Model):
    ip = models.GenericIPAddressField()
    user_agent = models.TextField()
    timpestamp = models.TimeField(default=now, editable=False)
    url = models.ForeignKey(URL)
    unsubscrive = models.BooleanField(default=False)

    @staticmethod
    def get_accesses_by_newsletter(newsletter_id):
        return Access.objects.filter(url__recipient__newsletter=newsletter_id) \
            .values('url__url').annotate(url=F('url__url'), hits=Count('id'))


class OpenTracker(models.Model):
    token = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    recipient = models.ForeignKey(Recipient, on_delete=models.CASCADE)
    open = models.BooleanField(default=False)

    def __str__(self):
        return str(self.token)
