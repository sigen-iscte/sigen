import base64

from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404

from Contacts.models import Contact
from url_redirect.models import Access, URL, OpenTracker


def redirect(request, token):
    url = get_object_or_404(URL, token=token)
    Access.objects.create(
        ip=request.META['REMOTE_ADDR'],
        user_agent=request.META['HTTP_USER_AGENT'],
        url=url,
        unsubscrive=url.is_unsubscrive(),
    )
    return HttpResponseRedirect(url.url)


def unsubscribe(request, token):
    contact = get_object_or_404(Contact, token=token)
    contact.deactivate()
    contact.save()
    return HttpResponse("O conataco {} sera removido das nossas listas".format(contact.email))


def open_track(request, token):
    o = OpenTracker.objects.get(token=token)
    o.open = True
    o.save()

    return HttpResponse(
        base64.b64decode('R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'),
        content_type='image/gif'
    )
