from django.conf.urls import url

from url_redirect import views

urlpatterns = [
    url(r'redirect/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})',
        views.redirect,
        name='redirect'),
    url(r'unsubscribe/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})',
        views.unsubscribe,
        name='unsubscribe'),
    url(r'open/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}).gif',
        views.open_track,
        name='open_track'),
]
