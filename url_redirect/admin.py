from django.contrib import admin

from url_redirect.models import URL, Access


@admin.register(URL)
class URLAdmin(admin.ModelAdmin):
    pass


@admin.register(Access)
class AccessAdmin(admin.ModelAdmin):
    pass
