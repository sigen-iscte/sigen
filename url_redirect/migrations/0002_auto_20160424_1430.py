# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-24 14:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('Newsletter', '0006_auto_20160423_1331'),
        ('url_redirect', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='url',
            name='contact',
        ),
        migrations.AddField(
            model_name='url',
            name='recipient',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Newsletter.Recipient'),
        ),
        migrations.AlterField(
            model_name='access',
            name='timpestamp',
            field=models.TimeField(default=django.utils.timezone.now, editable=False),
        ),
        migrations.AlterField(
            model_name='url',
            name='token',
            field=models.UUIDField(default=uuid.uuid4, editable=False),
        ),
    ]
