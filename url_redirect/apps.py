import re

from bs4 import BeautifulSoup
from django.apps import AppConfig
from django.core.urlresolvers import reverse

from url_redirect.models import URL, OpenTracker


class UrlRedirectConfig(AppConfig):
    name = 'url_redirect'


def inject_redirect(recipient, body):
    doc = BeautifulSoup(body, 'html.parser')
    for a in doc.find_all('a'):
        url = URL.objects.create(url=a['href'], recipient=recipient)
        a['href'] = reverse('redirect', args=(url.token,))

    return str(doc)


def inject_unsubcrive(recipient, body):
    find = re.findall(r'(?P<token>\[unsubscribe-link(?::(?P<value>.+))?])', body)
    for l in find:
        url = URL.objects.create(
            url=reverse('unsubscribe', args=(recipient.contact.token,)),
            recipient=recipient,
            unsubscrive=True,
        )

        doc = BeautifulSoup(body, 'html.parser')
        a = doc.new_tag(name='a')
        a['href'] = reverse('redirect', args=(url.token,))
        a.string = l[1]
        body = body.replace(l[0], str(a), 1)

    return body


def inject_tracking_image(recipient, body):
    open_tracker = OpenTracker.objects.create(recipient=recipient)
    doc = BeautifulSoup(body, 'html.parser')
    img = doc.new_tag('img')
    img['src'] = reverse('open_track', args=(open_tracker.token,))
    return body + str(img)
