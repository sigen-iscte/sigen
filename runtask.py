import random
from celery import signature
from kombu import Queue, Exchange

from tasks import add, app, send_email_limited


# NewsletterTask.apply_async(args=(1, 2), rate_limit='1/m', queue='ratedQueue')

def rand():
    return random.randint(1, 100)

send_email_limited.apply_async((10, 10))

# add.apply_async((rand(), rand()), rate_limit='1/h', queue='ratedQueue')

# q = Queue(
#     'anotherQueue',
#     Exchange('anotherQueue', 'direct', durable=True),
#     durable=True
# )
# print(q)

# add.apply_async(
#     (rand(), rand()),
#     queue="AnotherQueue"
# )

# app.control.add_consumer("ratedQueue", reply=True, destination=['worker1.local'])

# rated1 = make_rated_task('aRatedNewsletter', '1/min')
# rated1.apply_async(
#     args=(random.randint(1, 100), random.randint(1, 100)),
#     queue='ratedQueue'
# )

# print(app.control.inspect().active_queues())
