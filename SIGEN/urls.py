"""SIGEN URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

import Contacts.Import.urls
import Contacts.urls
import Newsletter.urls
import url_redirect.urls
from SIGEN import settings
from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.coming_soon, name='home'),
    url(r'^', include(Contacts.Import.urls)),
    url(r'^', include(Contacts.urls)),
    url(r'^', include(Newsletter.urls)),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^ckeditor', include('ckeditor_uploader.urls')),
    url(r'^', include(url_redirect.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static('media', document_root=settings.MEDIA_ROOT)
