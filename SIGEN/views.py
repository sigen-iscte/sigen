from django.shortcuts import redirect


def coming_soon(request):
    if request.user.is_authenticated():
        return redirect('newsletters')
    else:
        return redirect('login')
