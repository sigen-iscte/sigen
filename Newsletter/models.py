import uuid

from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.db import models, connection
from django.db.models.expressions import F
from django.utils import timezone

from Contacts.models import ContactList, Contact


class Settings(models.Model):
    owner = models.ForeignKey(User)
    fail_to_inactive_threshold = models.PositiveSmallIntegerField(null=True, default=None)


# TODO encrypt username and password?
class Transport(models.Model):
    name = models.CharField(max_length=255)
    host = models.CharField(max_length=255)
    port = models.CharField(max_length=10)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    sends_per_minute = models.PositiveSmallIntegerField(null=True)
    owner = models.ForeignKey(User)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.name


class Newsletter(models.Model):
    STATUS_NEW = 'New'
    STATUS_SENDING = 'Sending'
    STATUS_SENT = 'Sent'
    STATUS_ABORTED = 'Aborted'
    STATUS_FAILED = 'Failed'
    STATUSES = (
        (STATUS_NEW, STATUS_NEW),
        (STATUS_SENDING, STATUS_SENDING),
        (STATUS_SENT, STATUS_SENT),
        (STATUS_ABORTED, STATUS_ABORTED),
        (STATUS_FAILED, STATUS_FAILED),
    )
    contact_list = models.ForeignKey(ContactList, on_delete=models.SET_NULL, null=True)
    transport = models.ForeignKey(Transport, on_delete=models.SET_NULL, null=True)
    recipients = models.ManyToManyField(Contact, through='Recipient')
    from_email = models.EmailField()
    subject = models.CharField(max_length=255)
    body = RichTextUploadingField(null=True)
    body_plain = models.TextField(null=True)
    task_id = models.CharField(max_length=255, null=True)
    status = models.CharField(max_length=30, choices=STATUSES, default=STATUS_NEW)
    sending_started_date = models.DateTimeField(null=True)
    sending_finished_date = models.DateTimeField(null=True)
    sending_schedule_date = models.DateTimeField(null=True, default=None)
    sending_schedule_date_utc = models.DateTimeField(null=True, default=None)
    owner = models.ForeignKey(User)

    class Meta:
        ordering = ['-id']

    def create_recipients(self):
        if self.recipients.count():
            return
        query = 'INSERT INTO Newsletter_recipient (contact_id, newsletter_id, status, activity_status, retries) ' \
                'SELECT id, %d, "%s", "%s", 0 ' \
                'FROM Contacts_contact ' \
                'WHERE contact_list_id = %d AND active = 1' \
                % (self.id, Recipient.STATUS_NEW, Recipient.ACTIVITY_UNOPENED, self.contact_list_id)
        with connection.cursor() as c:
            c.execute(query)

    def ready_to_send(self):
        return self.contact_list is not None and\
               self.transport is not None and\
               self.status != self.STATUS_SENDING

    def abort(self):
        self.status = self.STATUS_ABORTED
        self.sending_finished_date = timezone.now()

    def start(self):
        self.create_recipients()
        self.status = self.STATUS_SENDING
        self.sending_started_date = timezone.now()

    def prepare_to_send(self):
        self.task_id = str(uuid.uuid4())

    def finish(self):
        self.status = self.STATUS_SENT
        self.sending_finished_date = timezone.now()

    def is_new(self):
        return self.status == self.STATUS_NEW

    def is_aborted(self):
        return self.status == self.STATUS_ABORTED

    def is_sending(self):
        return self.status == self.STATUS_SENDING

    def is_sent(self):
        return self.status == self.STATUS_SENT

    def __str__(self):
        return self.subject

    @staticmethod
    def get_newsletters_for_contact(contact_id):
        return Newsletter.objects.filter(recipient__contact=contact_id)\
            .values('id', 'subject', 'recipient__status')\
            .annotate(recipient_status=F('recipient__status'))


class Recipient(models.Model):
    STATUS_NEW = 'New'
    STATUS_SENT = 'Sent'
    STATUS_RETRYING = 'Retrying'
    STATUS_FAILED = 'Failed'
    STATUSES = (
        (STATUS_NEW, 'New'),
        (STATUS_SENT, 'Sent'),
        (STATUS_RETRYING, 'Retrying'),
        (STATUS_FAILED, 'Failed'),
    )

    ACTIVITY_UNOPENED = 'Unopened'
    ACTIVITY_OPENED = 'Opened'
    ACTIVITY_CLICKED = 'Clicked'
    ACTIVITY_UNSUBSCRIBE = 'Unsubscribe'
    ACTIVITY_STATUSES = (
        (ACTIVITY_UNOPENED, 'Unopened'),
        (ACTIVITY_OPENED, 'Opened'),
        (ACTIVITY_CLICKED, 'Clicked'),
        (ACTIVITY_UNSUBSCRIBE, 'Unsubscribe'),
    )

    newsletter = models.ForeignKey(Newsletter, on_delete=models.CASCADE)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    sent_date = models.DateTimeField(null=True)
    status = models.CharField(max_length=30, choices=STATUSES)
    activity_status = models.CharField(max_length=30, choices=ACTIVITY_STATUSES, default=ACTIVITY_UNOPENED)
    retries = models.PositiveSmallIntegerField(default=0)

    def sent(self):
        self.sent_date = timezone.now()
        self.status = self.STATUS_SENT

    def retrying(self):
        self.status = self.STATUS_RETRYING
        self.retries += 1

    def failed(self):
        self.status = self.STATUS_FAILED

    def opened(self):
        if self.activity_status == self.ACTIVITY_UNOPENED:
            self.activity_status = self.ACTIVITY_OPENED

    def clicked(self):
        self.activity_status = self.ACTIVITY_CLICKED

    def unsubscribe(self):
        self.activity_status = self.ACTIVITY_UNSUBSCRIBE

    def __str__(self):
        return '%s - %s' % (self.newsletter.subject, self.contact.email)

    @staticmethod
    def get_number_of_fails_for_contact(contact_id):
        return Recipient.objects.filter(contact_id=contact_id, status=Recipient.STATUS_FAILED)\
            .count()
