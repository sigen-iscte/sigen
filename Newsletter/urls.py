from django.conf.urls import url, include

import Newsletter.views

urlpatterns = [
    url(r'^transports/edit/(\d+)?', Newsletter.views.edit_transport, name='transport'),
    url(r'^transports/remove/(\d+)$', Newsletter.views.transports_remove, name='transports-remove'),
    url(r'^transports', Newsletter.views.transports, name='transports'),
    url(r'^newsletters/status-update/(\d+)', Newsletter.views.newsletter_status_update, name='newsletter-status-update'),
    url(r'^newsletters/status/(\d+)', Newsletter.views.newsletter_sending_status, name='newsletter-status'),
    url(r'^newsletters/activity/(\d+)', Newsletter.views.newsletter_activity, name='newsletter-activity'),
    url(r'^newsletters/report/(\d+)', Newsletter.views.newsletter_report, name='newsletter-report'),
    url(r'^newsletters/duplicate/(\d+)', Newsletter.views.newsletter_duplicate, name='newsletter-duplicate'),
    url(r'^newsletters/edit/(\d+)?', Newsletter.views.newsletter_edit, name='newsletter'),
    url(r'^newsletters/send/(\d+)/(\d)', Newsletter.views.send_newsletter, name='send-newsletter-force'),
    url(r'^newsletters/send/(\d+)', Newsletter.views.send_newsletter, name='send-newsletter'),
    url(r'^newsletters/cancel/(\d+)', Newsletter.views.cancel_newsletter, name='cancel-newsletter'),
    url(r'^newsletters/$', Newsletter.views.newsletters, name='newsletters'),
    url(r'^newsletters/settings$', Newsletter.views.settings_edit, name='newsletters-settings'),
]
