from django import template
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

from Newsletter.models import Newsletter

register = template.Library()


@register.filter
def newsletter_status(newsletter):
    def success():
        return '<span class="label label-success">%s</span>' % newsletter.status

    def warning():
        return '<span class="label label-warning">%s</span>' % newsletter.status

    def primary():
        return '<span class="label label-primary">%s</span>' % newsletter.status

    def default():
        return '<span class="label label-default">%s</span>' % newsletter.status

    def danger():
        return '<span class="label label-danger">%s</span>' % newsletter.status

    def sending():
        return '<a href="%s">%s</a>' % (reverse('newsletter-status', args=(newsletter.pk,)), primary())

    switcher = {
        Newsletter.STATUS_SENT: success,
        Newsletter.STATUS_ABORTED: warning,
        Newsletter.STATUS_SENDING: sending,
        Newsletter.STATUS_NEW: default,
        Newsletter.STATUS_FAILED: danger,
    }

    return mark_safe(switcher.get(newsletter.status)())
