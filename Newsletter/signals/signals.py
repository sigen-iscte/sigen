from django import dispatch

recipient_failed = dispatch.Signal(providing_args=['recipient', 'newsletter'])
