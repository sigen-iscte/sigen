from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver

from Newsletter.models import Settings
from Newsletter.signals.signals import recipient_failed
from url_redirect.models import Access, OpenTracker


@receiver(post_save, sender=User)
def create_configuration_for_user(sender, **kwargs):
    if kwargs.get('created', False):
        user = kwargs.get('instance')
        Settings.objects.create(owner=user)


@receiver(post_save, sender=Access)
def redirect_listener(sender, **kwargs):
    if kwargs.get('created', False):
        url = kwargs.get('instance').url
        recipient = url.recipient
        if url.is_unsubscrive():
            recipient.unsubscribe()
        else:
            recipient.clicked()
        recipient.save()


@receiver(post_save, sender=OpenTracker)
def open_listener(sender, **kwargs):
    if kwargs.get('created', False):
        recipient = kwargs.get('instance').recipient
        recipient.opened()
        recipient.save()


@receiver(recipient_failed)
def recipient_failed_listener(sender, **kwargs):
    recipient = kwargs.get('recipient')
    newsletter = kwargs.get('newsletter')
    max_fails = Settings.objects.get(owner=newsletter.owner).fail_to_inactive_threshold
    if not max_fails:
        return
    fails = recipient.get_number_of_fails_for_contact(recipient.contact_id)
    if fails >= max_fails:
        contact = recipient.contact
        contact.deactivate()
        contact.save()
