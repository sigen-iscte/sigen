from django.contrib import admin

from Newsletter.models import Newsletter, Recipient, Transport


@admin.register(Recipient)
class RecipientAdmin(admin.ModelAdmin):
    pass


@admin.register(Transport)
class TransportAdmin(admin.ModelAdmin):
    pass


@admin.register(Newsletter)
class NewsletterAdmin(admin.ModelAdmin):
    readonly_fields = ( 'sending_started_date', 'sending_finished_date')
    #readonly_fields = ('status', 'sending_started_date', 'sending_finished_date')
