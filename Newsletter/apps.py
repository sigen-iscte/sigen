from django.apps import AppConfig


class NewsletterConfig(AppConfig):
    name = 'Newsletter'

    def ready(self):
        import Newsletter.signals.handlers
