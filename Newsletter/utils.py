from smtplib import SMTPResponseException

from django.core.mail import get_connection


def open_connection_or_none(host, port, username, password):
    connection = get_connection(host=host, port=port, username=username, password=password)
    try:
        connection.open()
        return connection
    except SMTPResponseException:
        return None
