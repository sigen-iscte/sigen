from ckeditor.widgets import CKEditorWidget
from datetimewidget.widgets import DateTimeWidget
from django import forms
from django.forms import ModelForm

from Contacts.models import ContactList
from Newsletter.models import Transport, Newsletter, Settings


class EditTransport(ModelForm):
    class Meta:
        model = Transport
        widgets = {
            'password': forms.PasswordInput(render_value=True)
        }
        fields = ('name', 'host', 'port', 'username', 'password', 'sends_per_minute')
        labels = {
            'name': 'Nome',
            'host': 'Domínio',
            'port': 'Porto',
            'username': 'Nome de utilizador',
            'password': 'Password',
            'sends_per_minute': 'Nº de envios por minuto',
        }


class EditSettings(ModelForm):
    class Meta:
        model = Settings
        fields = ('fail_to_inactive_threshold',)
        labels = {
            'fail_to_inactive_threshold': 'Número de falhas de contacto até desactivá-lo',
        }


class EditNewsletter(ModelForm):

    def __init__(self, owner, *args, **kwargs):
        super(EditNewsletter, self).__init__(*args, **kwargs)
        self.fields['transport'].queryset = Transport.objects.filter(owner=owner)
        self.fields['contact_list'].queryset = ContactList.objects.filter(owner=owner)
        self.fields['sending_schedule_date'].widget = DateTimeWidget(
            attrs={'id': "schedule_date"}, usel10n=True, bootstrap_version=3)
        self.fields['sending_schedule_date'].required = False
        self.fields['sending_schedule_date_utc'].widget = forms.HiddenInput()
        self.fields['sending_schedule_date_utc'].required = False

    class Meta:
        model = Newsletter
        fields = ('subject', 'from_email', 'body', 'body_plain', 'sending_schedule_date',
                  'transport', 'contact_list', 'sending_schedule_date_utc')
        labels = {
            'subject': 'Assunto',
            'from_email': 'De email',
            'body': 'Corpo HTML',
            'body_plain': 'Corpo Texto',
            'sending_schedule_date': 'Agendar envio para',
            'transport': 'Transmissor',
            'contact_list': 'Lista de contactos',
        }
