from time import time

import eventlet
from celery.app import shared_task
from celery.contrib.abortable import AbortableTask
from celery.utils.log import get_task_logger
from django.core.mail import get_connection
from django.core.mail.message import EmailMultiAlternatives
from django.forms.models import model_to_dict
from django.utils.html import strip_tags
from django.utils.timezone import now

from Newsletter.models import Recipient, Newsletter
from Newsletter.signals.signals import recipient_failed
from url_redirect import apps

logger = get_task_logger(__name__)


@shared_task
def check_scheduled_newsletter():
    newsletters = Newsletter.objects.filter(sending_schedule_date_utc__lte=str(now()), status=Newsletter.STATUS_NEW)
    if newsletters.count():
        newsletter = newsletters[0]
        logger.info('scheduled newsletter found, starting sending task for newsletter of id %d' % newsletter.pk)
        newsletter.prepare_to_send()
        newsletter.save()
        send_email_limited.apply_async((newsletter.pk,), task_id=newsletter.task_id)


@shared_task(bind=True, base=AbortableTask)
def send_email_limited(
    self,
    newsletter_id,
    status=Recipient.STATUS_NEW,
    max_retries=3
):
    """
    :param self: AbortableTask
    :type newsletter_id: int
    :type status: basestring
    :type max_retries: int
    """
    newsletter = Newsletter.objects.get(pk=newsletter_id)
    recipients = Recipient.objects.filter(newsletter=newsletter_id, status=status)
    transport = newsletter.transport
    seconds_per_request = 60 / transport.sends_per_minute
    logger.info('seconds per request %d' % seconds_per_request)
    # "**" transforms dict to parameters
    connection = get_connection(**model_to_dict(transport, ('host', 'port', 'username', 'password')))
    connection.open()
    newsletter.start()
    newsletter.save()
    for recipient in recipients:
        if self.is_aborted():
            logger.info('Aborted!')
            break
        tries = 0
        send_count = 0
        while send_count == 0 and tries < max_retries:
            logger.info('sending to "%s", try %d' % (recipient, tries))
            start = time()

            msg = EmailMultiAlternatives(
                newsletter.subject,
                newsletter.body_plain or strip_tags(newsletter.body),
                newsletter.from_email,
                (recipient.contact.email,),
                connection=connection
            )
            body = newsletter.body
            body = apps.inject_tracking_image(recipient, body)
            body = apps.inject_redirect(recipient, body)
            body = apps.inject_unsubcrive(recipient, body)
            msg.attach_alternative(body, 'text/html')
            send_count = msg.send()
            elapsed = time() - start

            if elapsed < seconds_per_request:
                freeze_time = seconds_per_request - elapsed
                logger.info('Elapsed %f. Freezing for %f' % (elapsed, freeze_time))
                eventlet.sleep(freeze_time)

            tries += 1

            if send_count == 0:
                recipient.retrying()
                if tries == max_retries:
                    recipient.failed()
                    recipient_failed.send(sender=None, recipient=recipient, newsletter=newsletter)
            else:
                recipient.sent()

            recipient.save()

    if self.is_aborted():
        newsletter.abort()
    else:
        newsletter.finish()
    newsletter.save()
    connection.close()
    logger.info('done')
