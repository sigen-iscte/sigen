from celery.contrib.abortable import AbortableAsyncResult
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect

from Newsletter.forms import EditTransport, EditNewsletter, EditSettings
from Newsletter.models import Transport, Newsletter, Recipient, Settings
from Newsletter.tasks import send_email_limited
from Newsletter.utils import open_connection_or_none
from url_redirect.models import Access


@login_required
def newsletter_status_update(request, newsletter_id):
    newsletter = get_object_or_404(Newsletter, pk=newsletter_id, owner=request.user)
    recipient_count = newsletter.recipient_set.count()
    sent_recipient_count = newsletter.recipient_set.filter(status=Recipient.STATUS_SENT).count()
    failed_recipient_count = newsletter.recipient_set.filter(status=Recipient.STATUS_FAILED).count()

    return JsonResponse({
        'recipient_count': recipient_count,
        'recipient_sent_count': sent_recipient_count,
        'failed_recipient_count': failed_recipient_count,
        'recipient_to_send_count': recipient_count - sent_recipient_count,
        'sent_percentage': 100 * (sent_recipient_count / recipient_count),
    })


@login_required
def newsletter_activity(request, newsletter_id):
    newsletter = get_object_or_404(Newsletter, pk=newsletter_id, owner=request.user)
    results = [{
        'name': 'Recipients',
        'data': [newsletter.recipient_set.count()]
    }]
    results.extend([{
                        'name': status[0],
                        'data': [newsletter.recipient_set.filter(activity_status=status[0]).count()]
                    } for status in Recipient.ACTIVITY_STATUSES])
    return JsonResponse({'results': results})


@login_required
def newsletter_sending_status(request, newsletter_id):
    newsletter = get_object_or_404(Newsletter, pk=newsletter_id, owner=request.user)

    if newsletter.is_sent():
        return HttpResponseRedirect(reverse('newsletter-report', args=(newsletter_id,)))

    return render(request, 'Newsletter/newsletter/sending_status.html', {
        'newsletter': newsletter,
        'breadcrumb': [
            {
                'url': reverse('home'),
                'label': 'Home',
            },
            {
                'url': reverse('newsletters'),
                'label': 'Newsletters',
            },
            {
                'active': True,
                'label': 'Estado: %s' % newsletter.subject,
            },
        ],
    })


@login_required
def newsletter_report(request, newsletter_id):
    newsletter = get_object_or_404(Newsletter, pk=newsletter_id, owner=request.user)
    recipients = Recipient.objects.filter(newsletter=newsletter_id).select_related('contact')

    urls = Access.get_accesses_by_newsletter(newsletter_id)
    return render(request, 'Newsletter/newsletter/report.html', {
        'newsletter': newsletter,
        'recipients': recipients,
        'total_recipients': recipients.count(),
        'sent_recipients': recipients.filter(status=Recipient.STATUS_SENT).count(),
        'urls': urls,
        'breadcrumb': [
            {
                'url': reverse('home'),
                'label': 'Home',
            },
            {
                'url': reverse('newsletters'),
                'label': 'Newsletters',
            },
            {
                'active': True,
                'label': 'Relatório: %s' % newsletter.subject,
            },
        ],
    })


@login_required
def send_newsletter(request, newsletter_id, force=False):
    newsletter = get_object_or_404(Newsletter, pk=newsletter_id, owner=request.user)
    transport = newsletter.transport

    if not force and not newsletter.ready_to_send():
        return HttpResponse('Erro: Newsletter já enviada ou em envio')

    if not open_connection_or_none(**model_to_dict(transport, ('host', 'port', 'username', 'password',))):
        return HttpResponse('Erro ao ligar-se ao servidor de envio')

    newsletter.prepare_to_send()
    newsletter.save()
    send_email_limited.apply_async((newsletter.id,), task_id=newsletter.task_id)
    return redirect('newsletter-status', newsletter_id)


@login_required
def cancel_newsletter(request, newsletter_id):
    newsletter = get_object_or_404(Newsletter, pk=newsletter_id, owner=request.user)
    AbortableAsyncResult(newsletter.task_id).abort()
    return redirect('newsletter-report', newsletter_id)


@login_required
def edit_transport(request, transport_id=None):
    transport = Transport.objects.get(pk=transport_id) \
        if Transport.objects.filter(pk=transport_id).exists() \
        else Transport(owner=request.user)
    form = EditTransport(request.POST or None, instance=transport)

    if request.POST and form.is_valid():
        form.full_clean()
        form.save()
        return HttpResponseRedirect(reverse('transports'))

    return render(request, 'shared/centered_form.html', {
        'transport': transport,
        'form': form,
        'breadcrumb': [
            {
                'url': reverse('home'),
                'label': 'Home',
            },
            {
                'url': reverse('transports'),
                'label': 'Transmissores',
            },
            {
                'active': True,
                'label': 'Transmissor',
            },
        ],
    })


@login_required
def newsletter_edit(request, request_id=None):
    newsletter = Newsletter.objects.get(pk=request_id, owner=request.user) \
        if Newsletter.objects.filter(pk=request_id, owner=request.user).exists() \
        else Newsletter(owner=request.user)
    form = EditNewsletter(request.user, request.POST or None, instance=newsletter)

    if request.POST and form.is_valid():
        form.full_clean()
        form.save()
        return HttpResponseRedirect(reverse('newsletters'))

    return render(request, 'Newsletter/newsletter/edit_newsletter.html', {
        'newsletter': newsletter,
        'form': form,
        'breadcrumb': [
            {
                'url': reverse('home'),
                'label': 'Home',
            },
            {
                'url': reverse('newsletters'),
                'label': 'Newsletters',
            },
            {
                'active': True,
                'label': 'Newsletter: %s' % newsletter.subject,
            },
        ],
    })


@login_required
def settings_edit(request):
    settings = Settings.objects.get(owner=request.user) \
        if Settings.objects.filter(owner=request.user).exists() \
        else Settings(owner=request.user)
    form = EditSettings(request.POST or None, instance=settings)

    if request.POST and form.is_valid():
        form.full_clean()
        form.save()
        return redirect('newsletters')

    return render(request, 'shared/centered_form.html', {
        'form': form,
    })


@login_required
def newsletter_duplicate(request, newsletter_id):
    newsletter = get_object_or_404(Newsletter, pk=newsletter_id, owner=request.user)
    duplicate = Newsletter(
        owner=request.user,
        subject='Cópia de %s' % newsletter.subject,
        from_email=newsletter.from_email,
        body=newsletter.body,
        body_plain=newsletter.body_plain,
        contact_list=newsletter.contact_list,
        transport=newsletter.transport
    )
    duplicate.save()
    return redirect('newsletter', duplicate.pk)


@login_required
def transports(request):
    return render(request, 'Newsletter/transports.html', {
        'transports': Transport.objects.filter(owner=request.user),
        'breadcrumb': [
            {
                'url': reverse('home'),
                'label': 'Home',
            },
            {
                'active': True,
                'label': 'Transmissores',
            },
        ],
    })


@login_required
def newsletters(request):
    return render(request, 'Newsletter/newsletters.html', {
        'newsletters': Newsletter.objects.filter(owner=request.user),
        'breadcrumb': [
            {
                'url': reverse('home'),
                'label': 'Home',
            },
            {
                'active': True,
                'label': 'Newsletters',
            }
        ]
    })


@login_required
def transports_remove(request, transport_id):
    get_object_or_404(Transport, pk=transport_id, owner=request.user).delete()
    return HttpResponseRedirect(reverse('transports'))
