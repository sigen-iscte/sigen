CKEDITOR.plugins.add('Unsubscribe',
  {
    init: function (editor) {
      var pluginName = 'Unsubscribe';
      editor.ui.addButton('Unsubscribe',
        {
          label: 'Inserir link de desistência',
          command: 'OpenWindow',
          icon: CKEDITOR.plugins.getPath('Unsubscribe') + 'img.png'
        }
      );
      var cmd = editor.addCommand('OpenWindow', {exec: showMyDialog});
    }
  });
function showMyDialog(e) {
  e.insertHtml('[unsubscribe-link:Unsubscribe]');
}
