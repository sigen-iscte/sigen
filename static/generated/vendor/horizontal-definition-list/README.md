# Horizontal definition list

Adds classes and mixins to create an horizontal definition list.

## Installing with bower:

Run `bower i -S horizontal-definition-list=git@bitbucket.org:Battousai/horizontal-definition-list#~1.0.0` in your terminal.
