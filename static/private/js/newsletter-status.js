(function () {
  'use strict';
  var newsletterId = $('#NewsletterId').val();
  setInterval(function () {
    $.getJSON(
      '/newsletters/status-update/' + newsletterId,
      function (data) {
        $('#RecipientCount').html(data.recipient_count);
        $('#RecipientSentCount').html(data.recipient_sent_count);
        $('#RecipientFailedCount').html(data.failed_recipient_count);
        $('#RecipientToSendCount').html(data.recipient_to_send_count);
        $('#RecipientProgressBar').attr('style', 'width:' + data.sent_percentage + '%');
        if (data.recipient_count == data.recipient_sent_count) {
          window.location.href = '/newsletters/report/' + newsletterId;
        }
      }
    );
  }, 1000);
})();
