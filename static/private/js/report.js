(function () {
  $.getJSON('/newsletters/activity/' + $('#NewsletterId').val(), function (data) {
    new Highcharts.Chart({
      title: {
        text: 'Actividade'
      },
      chart: {
        renderTo: document.getElementById('Activity'),
        type: 'bar'
      },
      credits: {
        enabled: false
      },
      xAxis: {
        categories: ['Actividade'],
        title: {
          text: null
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: null
        }
      },
      series: data.results
    });
  });
})();
