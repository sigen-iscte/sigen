from time import time

import eventlet
from celery import Celery

app = Celery('tasks', broker='amqp://guest@localhost//')


@app.task
def send_email_limited(number_of_sends_per_minute, requests):
    seconds_per_request = 60 / number_of_sends_per_minute
    print('seconds per request {}'.format(seconds_per_request))

    for i in range(requests):
        print('request {}'.format(i))

        start = time()
        eventlet.sleep(1)
        elapsed = time() - start

        print('elapsed {}'.format(elapsed))
        if elapsed < seconds_per_request:
            freeze_time = seconds_per_request - elapsed
            print('Elapsed {}. Freezing for {}'.format(elapsed, freeze_time))
            eventlet.sleep(freeze_time)

        print('Done')


@app.task
def add(x, y):
    return x + y

# def make_rated_task(newsletter, rate):
#     """
#     :type newsletter: basestring
#     :type rate: basestring
#     """
#
#     name = newsletter + '_' + rate
#
#     @app.task(name=name, rate_limit=rate)
#     def add_limited(x, y):
#         return x + y
#
#     return add_limited
