'use strict';
var path = require('path');
var fs = require('fs');
var gulp = require('gulp');

var tasksDir = path.join(__dirname, 'gulp-tasks');
var conf = {
  devAssetsPath: 'static/private/',
  assetsPath: 'static/generated/',
  templatesPath: 'templates/',
  templatesExt: '.html',
  fontsPath: 'fonts/',
  jsPath: 'js/',
  imgPath: 'img/',
  sassPath: 'sass/',
  cssPath: 'css/',
  bsProxy: 'yoursite.dev',
};

fs.readdirSync(tasksDir).forEach(function (file) {
  if (file.split('.').pop() === 'js') {
    require(path.join(tasksDir, file))(gulp, conf);
  }
});
