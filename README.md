# SIGEN

## Requirements

See `requirements.txt`. Install with pip: `pip install -r /path/to/requirements.txt`.

[RabbitMQ](https://www.rabbitmq.com/) (`sudo apt-get install rabbitmq-server` in Debian/Ubuntu)

## Queues

To start Celery, run `python manage.py celeryd -E -B -l info`.

To have the django backend update, run `python manage.py celerycam`.

Optionally, Run `flower -A SIGEN` and access `http://localhost:5555` to monitor the running tasks.


## Tasks

Install node.js (`nvm` recommended in linux e osx).

Install Gulp and Bower globally (`npm i -g gulp`, `npm i -g bower`).

Install dependencies with `npm i` and `bower i`.

View available tasks with `gulp -T` and run the task with `gulp [nome da task]`.
